# -*- encoding: utf-8 -*-
$:.push File.expand_path("../lib", __FILE__)
require "jquery/datatables/rails/version"

Gem::Specification.new do |s|
  s.name = "jquery-datatables-rails"
  s.version = Jquery::Datatables::Rails::VERSION
  s.authors = ["Robin Wenglewski", "Alexander Reiff"]
  s.email = ["robin@wenglewski.de", "alexander.reiff@me.com"]
  s.homepage = "https://github.com/alexanderreiff/jquery-datatables-rails"
  s.summary = %q{jquery datatables for rails}
  s.description = %q{updated for use with Bootstrap 3}

  s.files = `git ls-files`.split("\n")
  s.files = Dir["{lib,vendor}/**/*"]
  s.test_files = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables = `git ls-files -- bin/*`.split("\n").map{ |f| File.basename(f) }
  s.require_paths = ["lib"]

  s.add_dependency "jquery-rails"

  s.add_development_dependency "bundler", "~> 1.7"
  s.add_development_dependency 'bundler-audit'
end
